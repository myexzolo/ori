$(function () {
  // $('#dateRageShow').hide();
  // $('.connectedSortable').sortable({
  //   containment         : $('section.content'),
  //   placeholder         : 'sort-highlight',
  //   connectWith         : '.connectedSortable',
  //   handle              : '.box-header, .nav-tabs',
  //   forcePlaceholderSize: true,
  //   zIndex              : 999999
  // });
  // $('.connectedSortable .box-header, .connectedSortable .nav-tabs-custom').css('cursor', 'move');
  //
  // // jQuery UI sortable for the todo list
  // $('.todo-list').sortable({
  //   placeholder         : 'sort-highlight',
  //   handle              : '.handle',
  //   forcePlaceholderSize: true,
  //   zIndex              : 999999
  // });
  //

  var role  = $('#role').val();
  if(role == 'admin')
  {
    checkApprove();

    setInterval(function(){
      checkApprove();
    }, 3000);
  }

  $('#daterage').daterangepicker(
    {
      locale: {
        format: 'DD/MM/YYYY',
        daysOfWeek: [
           "อา",
           "จ",
           "อ",
           "พ",
           "พฤ",
           "ศ",
           "ส"
       ],
       monthNames: [
           "มกราคม",
           "กุมภาพันธ์",
           "มีนาคม",
           "เมษายน",
           "พฤษภาคม",
           "มิถุนายน",
           "กรกฎาคม",
           "สิงหาคม",
           "กันยายน",
           "ตุลาคม",
           "พฤศจิกายน",
           "ธันวาคม"
       ]
      }
    }
  );

  searchProject();
});


// var gdpData = {
//   "TH-57": 200,
//   "TH-56": 100,
//   "TH-55": 50,
//   "TH-54": 200
// };


function checkApprove()
{
  $( document ).ajaxStart(function() {
    $(".loadingImg").addClass('none');
  });
  $.get("ajax/checkApprove.php")
     .done(function( data ) {
       //console.log(data);
       $("#checkApprove").html(data.count);
   });
}

function searchProject(){
  var daterage    = $('#daterage').val();
  var userLogin  = $('#user_login').val();
  var role  = $('#role').val();
  var res = daterage.split("-");
  var dateStart = dateThToEn(res[0].trim(),"dd/mm/yyyy","/");
  var dateEnd = dateThToEn(res[1].trim(),"dd/mm/yyyy","/");


  $.post("ajax/showProjectList.php",{dateStart:dateStart,dateEnd:dateEnd,userLogin:userLogin,role:role})
    .done(function( data ) {
      $('#show-form').html(data);
  });
}

function getMap()
{
  // $.post("ajax/class.php",{week:week})
  //   .done(function( data ) {
  //     $("#showClass").html( data );
  // });
  //ปกติ #00b16a
  //กลาง #00b16a
  //เยอะ #d91e18
  $('#map').vectorMap({
    map: 'th_mill',
    backgroundColor: 'transparent',
    regionStyle: {
                    initial: {
                      fill: '#8d8d8d'
                    }
                  },
    series: {
      regions: [{
        values: gdpData,
        scale: {"200": "#d91e18","100": "#f5e51b","50": "#00b16a"},
        normalizeFunction: 'polynomial'
      }]
    },
    onRegionTipShow: function(e, el, code){
      el.html(el.html()+' (จำนวน '+gdpData[code]+' ราย)');
    }

  });
}

function showClass(week)
{
  $.post("ajax/class.php",{week:week})
    .done(function( data ) {
      $("#showClass").html( data );
  });
}

function showClass1(week)
{
  $.post("ajax/class1.php",{week:week})
    .done(function( data ) {
      $("#showClass").html( data );
  });
}


function showClass2(week)
{
  $.post("ajax/class2.php",{week:week})
    .done(function( data ) {
      $("#showClass").html( data );
  });
}
