<?php
include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
?>
<style>
/* .btnList{
  cursor: pointer;
} */

.info-box-number {
    font-size: 24px;
}

.project-name {
    overflow: hidden;
    white-space: nowrap;
    text-overflow: ellipsis;
}

@media (max-width: 1400px) {
  .project-name {
      overflow: hidden;
      white-space: nowrap;
      text-overflow: ellipsis;
  }
}

@media (min-width:1400px)
{
  .col-lgg-4 {
    float: left
  }
  .col-lgg-4 {
    width: 33.33333333%
  }
}

.col-lgg-4 {
  position: relative;
  min-height: 1px;
  padding-right: 2px;
  padding-left: 2px
}

.tooltip-inner {
    max-width: 600px;
    padding: 3px 8px;
    color: #fff;
    text-align: center;
    background-color: #000;
    border-radius: 4px;
    font-size: 20px;
    font-family: "CSChatThai";
}

.btnList:hover {
  opacity: 0.9;
  -webkit-box-shadow:1px 1px 1px #de1dde;
 -moz-box-shadow:1px 1px 1px #de1dde;
  box-shadow:1px 1px 1px #de1dde;
}
</style>
<?php
  $con = "";
  $dateStart  = $_POST['dateStart'];
  $dateEnd    = $_POST['dateEnd'];
  $userLogin  = isset($_POST['userLogin'])?$_POST['userLogin']:"";
  $role       = isset($_POST['role'])?$_POST['role']:"";

  $dateStart = queryDateOracle($dateStart);
  $dateEnd   = queryDateOracle($dateEnd);

   //echo ">>".$_SESSION['ROLEACCESS'];
   //echo ">>".$_SESSION['ROLECODE'];
  // echo ">>".$_SESSION['USER_NAME'];

  if($userLogin != ""){
      $con = " and u.user_login = '".$userLogin."'";
  }


  $sql = "SELECT p.project_code, p.project_name, TO_CHAR(p.start_date, 'YYYY-MM-DD') as start_date  ,TO_CHAR(p.end_date, 'YYYY-MM-DD') as end_date,
          p.user_login, m.department_name,u.type_user,p.project_type
          FROM pfit_t_project p, t_user u
          LEFT JOIN pfit_t_member m ON u.user_id = m.user_id
          where p.status <> 'C' and p.user_login = u.user_login and p.start_date between $dateStart and  $dateEnd $con ORDER BY p.start_date";
  //echo $sql;
  $query = DbQuery($sql,null);
  $row  = json_decode($query, true);
  $num  = $row['dataCount'];
  $data = $row['data'];

  for ($i=0; $i < $num ; $i++) {
    $project_code = $data[$i]['project_code'];
    $project_name = $data[$i]['project_name'];
    $department_name = $data[$i]['department_name'];
    $project_type = $data[$i]['project_type'];
    $type_user = $data[$i]['type_user'];

    $pram = "?project_code=".$project_code."&project_name=".urlencode($project_name)."&project_type=".$project_type;

    if($type_user == "ADMIN")
    {
      $department_name = "กรมพลศึกษา";
    }
?>
  <div class="col-md-4 col-sm-6 col-xs-12">
    <div class="info-box bg-gray btnList" style="padding:10px;">
        <div class="info-box-number project-name" style="height:auto;"><span class="tooltipProject" title="<?= $project_name ?>"><?= $project_name ?></span></div>
        <?php if($role == "admin"){
        ?>
        <div style="font-size:21px;margin-top:10px;">
          หน่วยงาน : <?= $department_name; ?>
        </div>
        <?php } ?>

        <div style="font-size:18px;height:30px;margin-top:10px;">
          <?= DateThai($data[$i]['start_date']) ?> - <?= DateThai($data[$i]['end_date']) ?>
        </div>
        <div class="row" style="padding-right: 15px;padding-left: 15px;">
          <div class="col-sm-12 col-lgg-4">
            <button type="button" style="width: 100%;text-align: center;margin-bottom:5px;" class="btn btn-social bg-olive btn-flat btnList" onclick="postURL('<?= "../PFIT0101/index.php".$pram ?>')">
              <i class="fa fa-user-plus" style="font-size:18px;"></i> ผู้ทดสอบ
            </button>
          </div>
          <div class="col-sm-12 col-lgg-4">
            <button type="button" style="width: 100%;text-align: center;margin-bottom:5px;" class="btn btn-social bg-gray-active btn-flat" onclick="postURL('<?= "../PFIT0102/index.php".$pram ?>')">
              <i class="fa fa fa-floppy-o" style="font-size:18px;"></i> จัดเก็บผล</button>
          </div>
          <div class="col-sm-12 col-lgg-4">
            <button type="button" style="width: 100%;text-align: center;margin-bottom:5px;" class="btn btn-social bg-light-blue btn-flat" onclick="postURL('<?= "../PFIT0103/index.php".$pram ?>')">
              <i class="fa fa-file-text-o" style="font-size:18px;"></i> รายงาน</button>
          </div>
        </div>
    </div>
  </div>
<?php
}
if($num == 0){
?>
  <div class="col-xs-12">
    <div style="padding:15px;height:50px;">
      <p align="center">ไม่พบกิจกรรม</p>
    </div>
  </div>
<?php
}
?>
<script>
  $('.tooltipProject').tooltip();
</script>
