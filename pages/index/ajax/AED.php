<?php

include('../../../inc/function/mainFunc.php');
include('../../../inc/function/connect.php');

$hostPath = getUrlHost();

if(!isset($_REQUEST)){
  $postdata = file_get_contents("php://input");
  $request = json_decode($postdata,true);
}else{
  $request = $_REQUEST;
}

$action           = isset($request['action'])?$request['action']:"";
$typeDevice       = isset($request['typeDevice'])?$request['typeDevice']:"";

$member_name      = isset($request['member_name'])?$request['member_name']:"";
$member_lname     = isset($request['member_lname'])?$request['member_lname']:"";
$department_name  = isset($request['department_name'])?$request['department_name']:"";
$email            = isset($request['email'])?$request['email']:"";
$tel              = isset($request['tel'])?$request['tel']:"";
$objective        = isset($request['objective'])?$request['objective']:"";
$address          = isset($request['address'])?$request['address']:"";
$province_id      = isset($request['province_id'])?$request['province_id']:"";
$district_id      = isset($request['district_id'])?$request['district_id']:"";
$subdistrict_id   = isset($request['subdistrict_id'])?$request['subdistrict_id']:"";
$zipcode          = isset($request['zipcode'])?$request['zipcode']:"";

$user_login       = isset($request['user_login'])?$request['user_login']:"";
$password         = isset($request['user_password'])?$request['user_password']:"";
$user_name        = $member_name." ".$member_lname;
// $user_img   = isset($_POST['$request'])?$request['action']:"";

$user_password     = @md5($password);

$user_img   = "";
$document   = "";
$updateImg  = "";
$updateDoc  = "";

$user_id_update = 1;

$pathImg     = "../../../image/user/";
$pathUpload = "../../../upload/doc/";

$txt = "";
if($typeDevice == "WEB"){
    $regTxt = "REG WEB";
    $txt    = " ผ่านช่องทาง Website";
}else if($typeDevice == "MOBILE"){
    $regTxt = "REG MOBILE";
    $txt    = " ผ่านช่องทาง Mobile App";
}

if(isset($_FILES["user_img"]) && $typeDevice == "WEB")
{
  //$user_img = resizeImageToBase64($_FILES["user_img"],'256','256','100',$user_id_update,$path);
  $user_img = resizeImageToUpload($_FILES["user_img"],'256','256',$pathImg,$user_login);

  if($action == "EDIT" && $user_img != "")
  {
    $updateImg = "user_img = '$user_img',";
  }
}

if(isset($_FILES["document"]) && $typeDevice == "WEB")
{
  $document = uploadFile($_FILES["document"],$pathUpload,$user_login);

  if($action == "EDIT" && $document != "")
  {
    $updateDoc = "document = '$document',";
  }
}

if($action == 'ADD'){

  $sqlid  = "SELECT T_USER_SEQ.NEXTVAL AS next_id FROM DUAL";

  $query      = DbQuery($sqlid,null);
  $json       = json_decode($query, true);
  $user_id    = $json['data'][0]['next_id'];

  $sqlUser = "INSERT INTO t_user
             (user_id,user_login,user_name,user_password,is_active,role_list,user_img,user_id_update,note1,note2,pw,type_user)
             VALUES($user_id,'$user_login','$user_name','$user_password','R',
             '','$user_img','1','','','$password','MEMBER')";

  $query      = DbQuery($sqlUser,null);
  $json       = json_decode($query, true);
  $errorInfo  = $json['errorInfo'];

  if(intval($errorInfo[0]) == 0)
  {
    $sqlMember  = "INSERT INTO pfit_t_member
                   (user_id,member_name,member_lname,department_name,email,tel,
                    objective,address,province_id,district_id,subdistrict_id,zipcode,document)
                   VALUES
                   ('$user_id','$member_name','$member_lname','$department_name','$email','$tel',
                    '$objective','$address','$province_id','$district_id','$subdistrict_id','$zipcode','$document')";
  }
  else
  {
    header('Content-Type: application/json');
    exit(json_encode(array('status' => 'danger','message' => 'Fail')));
  }
}else if($action == 'EDIT'){
    $sqlMember = "UPDATE t_user SET
            user_login      = '$user_login',
            user_name       = '$user_name',
            is_active       = '$is_active',
            role_list       = '$role_list',
            $updateImg
            $updateDoc
            note1           = '$note1',
            note2           = '$note2',
            user_id_update  = '$user_id_update'
            WHERE user_id   = $user_id";

    if($user_id_update == $user_id){
      $sqls   = "SELECT * FROM t_user WHERE user_id = '$user_id'";
      $query      = DbQuery($sqls,null);
      $json       = json_decode($query, true);
      $rows       = $json['data'];

      $date_login = $_SESSION['member'][0]['date_login'];
      $rows[0]['date_login'] = $date_login;
      $_SESSION['member'] = $rows;
    }

}

$query      = DbQuery($sqlMember,null);
$row        = json_decode($query, true);
//print_r($row);
$errorInfo  = $row['errorInfo'];

if(intval($errorInfo[0]) == 0){

  $message   = "\r\n"."มีผู้ลงทะเบียน ".$txt."\r\n";
  $message  .= "ชื่อผู้ลงทะเบียน : ".$user_name."\r\n";
  $message  .= "หน่วยงาน : ".$department_name."\r\n";
  $message  .= "เบอร์โทรติดต่อ : ".$tel."\r\n";
  $message  .= "E-mail : ".$email."\r\n";
  $message  .= "สถานะ : รอตรวจสอบข้อมูล"."\r\n";
  $message  .= "Link : ".$hostPath."pages/login/";


  $token = GET_LINE_TOKEN("");

  LINE_NOTIFY_MESSAGE($message,"",$token);

  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'success','message' => 'Success')));
}else{
  header('Content-Type: application/json');
  exit(json_encode(array('status' => 'danger','message' => 'Fail')));
}


?>
