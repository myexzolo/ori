<div class="box-body" style="height:500px;overflow-y: scroll;">
<ul class="products-list product-list-in-box">
<?php
include('../../../inc/function/connect.php');
include('../../../inc/function/mainFunc.php');
header("Content-type:text/html; charset=UTF-8");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);

$sql        = "SELECT * FROM pfit_t_media where is_active = 'Y' and type_media = 'V' ORDER BY update_date DESC";
$query      = DbQuery($sql,null);
$json       = json_decode($query, true);
$rows       = $json['data'];
$count      = $json['dataCount'];

for ($i=0; $i < $count ; $i++)
{
  $src = "https://www.youtube.com/embed/".$rows[$i]['path'];
  $pathUpload = "../../upload/ebook/".$rows[$i]['path'];
?>
    <li class="item">
      <div class="product-img">
        <iframe width="160" height="90" src="<?=$src ?>"></iframe>
      </div>
      <div class="product-info" style="margin-left: 180px;">
        <a href="<?=$pathUpload?>" class="product-title" target="_blank"> <?= $rows[$i]['title']; ?>
      </div>
    </li>
<?php
}
?>
</ul>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default btn-flat" style="width:100px;" data-dismiss="modal">ปิด</button>
</div>
