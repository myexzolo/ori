function showFormReg(){
  $.post("ajax/formReg.php",{})
    .done(function( data ) {
      $('#myModalReg').modal({backdrop:'static'});
      $('#show-form-reg').html(data);
  });
}


function showFormMedia(type){
  var url = "";
  if(type == "E"){
    url = "ajax/formEbook.php";
  }else if(type == "V")
  {
    url = "ajax/formClip.php";
  }
  
  $.post(url,{type:type})
    .done(function( data ) {
      $('#myModalMedia').modal({backdrop:'static'});
      $('#show-form-media').html(data);
  });
}


function loadExcel(url)
{
  postURL_blank(url);
  $('#myModalExcel').modal('toggle');
}

function exportExcel()
{
  $('#myModalExcel').modal({backdrop:'static'});
}

function postURL_blank(url) {
 var form = document.createElement("FORM");
 form.method = "POST";
 form.target = "_blank";
 form.style.display = "none";
 document.body.appendChild(form);
 form.action = url.replace(/\?(.*)/, function(_, urlArgs) {
   urlArgs.replace(/\+/g, " ").replace(/([^&=]+)=([^&=]*)/g, function(input, key, value) {
     input = document.createElement("INPUT");
     input.type = "hidden";
     input.name = decodeURIComponent(key);
     input.value = decodeURIComponent(value);
     form.appendChild(input);
   });
   return "";
 });
 //console.log(form);
 form.submit();
}

$('#formReg').on('submit', function(event) {
  event.preventDefault();
  if ($('#formReg').smkValidate()) {
    if( $.smkEqualPass('#pass1', '#pass2') ){
      $.ajax({
          url: 'ajax/AED.php',
          type: 'POST',
          data: new FormData( this ),
          processData: false,
          contentType: false,
          dataType: 'json'
      }).done(function( data ) {
        if(data.status = "success")
        {
          $.smkAlert({
            text: 'ลงทะเบียนสำเร็จ รอเจ้าหน้าที่ตรวจสอบข้อมูล ผลการตวจสอบจะถูกส่งทาง Email ที่ได้ลงทะเบียนไว้',
            type: 'success',
            position:'top-center'
          });

        }else{
          $.smkAlert({
            text: 'ลงทะเบียนไม่สำเร็จ ติดต่อเจ้าหน้าที่ ที่ Line@ : https://lin.ee/2xuqYFT6A',
            type: 'danger',
            position:'top-center'
          });
        }
        $('#myModalReg').modal('toggle');
        // $.smkProgressBar({
        //   element:'body',
        //   status:'start',
        //   bgColor: '#fff',
        //   barColor: '#242d6d',
        //   content: 'Loading...'
        // });
        // setTimeout(function(){
        //   $.smkProgressBar({status:'end'});
        //   $('#formReg').smkClear();
        //   showTable();
        //   showSlidebar();
        //   $.smkAlert({text: data.message,type: data.status});
        //   $('#myModal').modal('toggle');
        // }, 1000);
      });
    }
  }
});

function checkUserCode(){
  var text = $('#user_login').val();
  $.post("ajax/checkUserCode.php",{UserCode:text})
  .done(function( data ) {
      //console.log(data);
      if(data.status)
      {
        $.smkAlert({
          text: 'User Login ซ้ำ !!',
          type: 'danger',
          position:'top-center'
        });
        $('#user_login').val('');
        $('#user_login').focus();
      }
    });
  }

function getProvince(id,type){
  $.post("ajax/provinces.php",{id:id,type:type})
    .done(function( data ) {
        $('#district').html(data);
        getDistrict($('#district').val(),'D');
  });
}

function getDistrict(id,type){
  $.post("ajax/provinces.php",{id:id,type:type})
    .done(function( data ) {
        $('#subdistrict').html(data);
        getSubDistrict($('#subdistrict').val(),'SD');
  });
}

function getSubDistrict(id,type){
  $.post("ajax/provinces.php",{id:id,type:type})
    .done(function( data ) {
        $('#zipcode').val(data.zip_code);
  });
}
